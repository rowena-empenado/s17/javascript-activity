/* Add student */

let studentsList = [];
let studentToAdd = ['John', 'Jane', 'Joe']

studentToAdd.forEach(
	function(studentToAdd){
		studentsList.push(studentToAdd)
		console.log(`addStudent(${studentToAdd})`)
		console.log(`${studentToAdd} was added to the student's list.`)
		console.log(' ');
	}
)


/* Count student */

let count = studentsList.length

function countStudents() {
	console.log(`countStudents()`)
	console.log(`There are a total of ${count} students enrolled.`)
	console.log(' ');
}
countStudents();


/* Print student */

function printStudents() {
	console.log(`printStudents()`)
	studentsList.sort()
	studentsList.forEach(
		function(student){
			console.log(student)
		}
	)
	console.log(' ');
}
printStudents();


/* Find student */

function findStudent(input) {
	console.log(`findStudent(${input})`)
	let filteredName = studentsList.filter(
				function(student) {
					return student.toLowerCase().includes(input.toLowerCase()) } )
	
	switch(true) {
		case (filteredName.length == 0):
			console.log(`No student found with the name ${input}`)
			break;
		case (filteredName.length == 1):
			console.log(`${filteredName} is an Enrollee`)
			break;
		case (filteredName.length > 1):
			console.log(`${filteredName} are enrollees`)
			break;
		default:
			break;
	}
	console.log(' ');
}
findStudent("joe");
findStudent("bill");
findStudent("j");


/* --------------------- STRETCH GOALS ---------------------------*/

/* Add section */
function addSection(section) {
	console.log(`addSection(${section})`)
	let studentSection = studentsList.map(
							function(student) {
								return `${student} - Section ${section}`
							}
						)
	console.log(studentSection);
	console.log(' ');
}
addSection('A');


/* Remove student */

function removeStudent(input) {
	let toBeRemoved = input[0].toUpperCase() + input.slice(1)
	let index = studentsList.indexOf(toBeRemoved)
	studentsList.splice(index,1)
	console.log(`removeStudent(${input})`)
	console.log(`${input} was removed from the student's list.`)
	console.log(' ')
}
removeStudent('joe')
printStudents()